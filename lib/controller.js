//----------------------------------------------------------------------------------------------------------------------
// FanController
//
// @module
//----------------------------------------------------------------------------------------------------------------------

const _ = require('lodash');
const smc = require('smc');
const libsmc = require('libsmc');

const { spawn } = require('child_process');

//----------------------------------------------------------------------------------------------------------------------

class FanController
{
    constructor(interval, minTemp)
    {
        this.interval = interval;
        this.minTemp = minTemp;
        this.maxTemp = 105;
        this.tempWindow = [];

        // The size of the window sample
        this.windowSize = 5;

        // Store data about our various fans
        this.fans = {};

        // Build the list by querying the SMC
        this._buildFans();
    } // end constructor

    //------------------------------------------------------------------------------------------------------------------
    // Internal
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Gets the average temperature across all 4 cpu cores. In theory, this could be smarter and check the number of
     * cores, but fuck it, I'm already over-engineering this.
     *
     * @returns {number}
     * @private
     */
    _getAvgCoreTemp()
    {
        const core1Temp = smc.get('TC1C') || 0;
        const core2Temp = smc.get('TC2C') || 0;
        const core3Temp = smc.get('TC3C') || 0;
        const core4Temp = smc.get('TC4C') || 0;

        return _.mean([ core1Temp, core2Temp, core3Temp, core4Temp ]) || 0;
    } // end _getAvgCoreTemp

    _getAvgTemp()
    {
        return _.mean(this.tempWindow);
    } // end _getAvgTemp

    _normalizeTemp(temp)
    {
        return (temp - this.minTemp) / (this.maxTemp - this.minTemp);
    } // end _normalizeTemp

    _buildFans()
    {
        const numFans = libsmc.getNumberOfFans();
        for(let idx = 0; idx < numFans; idx++)
        {
            const fanInfo = {
                id: `F${ idx }`,
                min: smc.get(`F${ idx }Mn`),
                max: smc.get(`F${ idx }Mx`),
                target: smc.get(`F${ idx }Tg`),
                actual: smc.get(`F${ idx }Ac`)
            };

            this.fans[fanInfo.id] = fanInfo;
        } // end for
    } // end _buildFans

    _updateTemp()
    {
        this.tempWindow.unshift(this._getAvgCoreTemp());
        if(this.tempWindow.length > this.windowSize)
        {
            this.tempWindow.length = this.windowSize;
        } // end if
    } // end _updateTemp

    _updateFans()
    {
        _.mapValues(this.fans, (fanInfo) =>
        {
            fanInfo.target = smc.get(`${ fanInfo.id }Tg`);
            fanInfo.actual = smc.get(`${ fanInfo.id }Ac`);

            return fanInfo;
        });
    } // end _updateFans

    _calcRPM(fanID, temp)
    {
        const fanInfo = this.fans[fanID];
        let rpmPercent = Math.tanh( 2 * this._normalizeTemp(temp));

        if(rpmPercent > 1)
        {
            rpmPercent = 1;
        } // end if

        if(rpmPercent < 0)
        {
            rpmPercent = 0;
        } // end if

        return fanInfo.setTo = Math.floor((fanInfo.max - fanInfo.min) * rpmPercent + fanInfo.min);
    } // end _calcRPM

    _setFansManual()
    {
        spawn('/Applications/smcFanControl.app/Contents/Resources/smc', ['-k', 'FS! ', '-w', '0003']);
    } // end _setFansManual

    _setFansAuto()
    {
        spawn('/Applications/smcFanControl.app/Contents/Resources/smc', ['-k', 'FS! ', '-w', '0000']);
    } // end _setFansAuto

    _setFanRPM(fanID, target)
    {
        const rpmBytes = (target << 2).toString(16);
        spawn('/Applications/smcFanControl.app/Contents/Resources/smc', ['-k', `${ fanID }Tg`, '-w', rpmBytes]);
    } // end _setFanRPM

    //------------------------------------------------------------------------------------------------------------------
    // Monitor Loop
    //------------------------------------------------------------------------------------------------------------------

    onLoop()
    {
        // Update data
        this._updateFans();
        this._updateTemp();

        // Display crap
        const temp = this._getAvgTemp();

        // Calculate the RPMs for each fan
        const f0RPM = this._calcRPM('F0', temp);
        const f1RPM = this._calcRPM('F1', temp);

        // Set the fan RPM
        this._setFanRPM('F0', f0RPM);
        this._setFanRPM('F1', f1RPM);
    } // end onLoop

    //------------------------------------------------------------------------------------------------------------------
    // Public API
    //------------------------------------------------------------------------------------------------------------------

    start()
    {
        this._setFansManual();
        this.clearHandle = setInterval(this.onLoop.bind(this), this.interval);
    } // end start

    stop()
    {
        this._setFansAuto();
        clearInterval(this.clearHandle);
    } // end stop
} // end FanController

//----------------------------------------------------------------------------------------------------------------------

module.exports = FanController;

//----------------------------------------------------------------------------------------------------------------------
