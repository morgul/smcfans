# SMCFans

> A simple fan controller for Macbooks. Or, "What you do when baking your motherboard leads to useless automatic fan control."

Basically, this is a hacked together script that controls the fans in my macbook, calculating the RPM based on the
current temperature. The RPM ramps up the closer to 105C you get to. Normally, it hits 95% full speed around 80C.

## How to use

Just run `npm start` and it will run.

## How to install

I've added the stuff needed to install in `launchd`. It's pretty simple:

```bash
$ npm run install
> sudo password: *****
$ npm run load
```

You will need to put in your user password.

Also, good to note: the `plist` file assumed the script is in my home directory. It doesn't actually install the node
code anywhere... meh, lazy.

## How to stop the service

Assuming you've installed it, just do:

```bash
$ npm run unload
> sudo password: *****
```

## Uninstall

Also very simple:

```bash
$ npm run unload
> sudo password: *****
$ npm run uninstall
```
